package com.solab.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Spring Boot entry point.
 *
 * @author Enrique Zamudio
 *         Date: 3/13/17 7:07 PM
 */
@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class Application {

    @Bean(initMethod = "run")
    public AsyncServer protoServer(Lookups handler, Environment env) {
        return new AsyncServer(
                env.getProperty("proto.port", Integer.class, 8001),
                env.getProperty("proto.threads", Integer.class,
                        Runtime.getRuntime().availableProcessors()),
                handler);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
