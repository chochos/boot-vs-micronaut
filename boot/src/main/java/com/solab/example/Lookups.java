package com.solab.example;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicLong;

import com.solab.example.protos.MaxProto;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
//This annotation makes values from application.properties available via @Value
@ConfigurationProperties
public class Lookups {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public void ipLookup(String ip, MaxProto.Response.Builder resp) {
        InetAddress ipAddress;
        try {
            ipAddress = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            log.error("Lookup: Unknown host {}", ip);
            resp.setError("UnknownHostException: " + e.getMessage());
            return;
        }

        MaxProto.Location.Builder loc = MaxProto.Location.newBuilder();
        loc.setCountryIso("MX").setCountryName("Mexico");
        loc.setCityName("CDMX");
        loc.setPostal("11800");
        resp.setLoc(loc.build());
    }

}
